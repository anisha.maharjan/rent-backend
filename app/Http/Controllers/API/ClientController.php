<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Client;

use App\Model\Room;

class ClientController extends Controller
{
    public $successStatus = 200;

    public function allClients(){
        $clients = Client::with('room')->get();

        return response()->json(["clients" => $clients], $this->successStatus);
    }

    public function addClientToRoom(){
        $data = Client::select('id', 'room_id')->orderBy('created_at', 'desc')->first();
        $room = Room::find($data->room_id);
        $room->client_id = $data->id;
        $room->status = "occupied";
        $room->save();
    }

    public function addClient(Request $request){
        $this->validate($request, [
            "name" => "required",
            "joined_date" => "required|date",
            "room_id" => "required"
        ]);

        $client = Client::create([
            "name" => $request->name,
            "joined_date" => $request->joined_date,
            "room_id" => $request->room_id
        ]);

        $client->save();

        $this->addClientToRoom();

        return response()->json(['success' => "Client added successfully."], $this->successStatus);
    }

}
