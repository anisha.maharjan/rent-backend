<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Room;

class RoomController extends Controller
{
    public $successStatus = 200;

    public function allRooms(){
        $rooms = Room::all();

        return response()->json(['rooms' => $rooms], $this->successStatus);
    }

    public function freeRooms(){
        $rooms = Room::where("status" , "free")->get();

        return response()->json(['rooms' => $rooms], $this->successStatus);
    }
}
