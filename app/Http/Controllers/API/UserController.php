<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public $successStatus = 200;

    // current user details
    public function currentUserDetails() { 
        $user = Auth::user(); 
        return response()->json(['user' => $user], $this-> successStatus); 
    } 

    // edit user detail
    public function editUser($id){
        $user = USer::find($id);
        error_log($user);
    }

    // get all users details
    public function allUsers(){
        $users = User::all();
        return response()->json(['users' => $users], $this-> successStatus); 
    }
}
