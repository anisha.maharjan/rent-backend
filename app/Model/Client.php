<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Client extends Model
{
    protected $table = 'clients';

    public function room()
    {
        return $this->belongsTo('App\Model\Room');
    }

    protected $fillable = ['id','name','room_id','joined_date','left_date'];
}
