<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Room extends Model
{
    protected $table = 'rooms';

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
