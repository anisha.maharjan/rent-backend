<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Auth
Route::post('login', 'API\AuthController@login');
Route::post('register', 'API\AuthController@register');
Route::get('logout', 'API\AuthController@logout')->middleware('auth:api');

// User
Route::middleware('auth:api')->group(function () {
    Route::get('currentUserDetails', 'API\UserController@currentUserDetails');
    Route::get('allUsers', 'API\UserController@allUsers');
});

// Room
Route::middleware('auth:api')->group(function () {
    Route::get('allRooms', 'API\RoomController@allRooms');
    Route::get('freeRooms', 'API\RoomController@freeRooms');
});

// Client
Route::middleware('auth:api')->group(function () {
    Route::get('allClients', 'API\ClientController@allClients');
    Route::post('addClient', 'API\ClientController@addClient');
});
